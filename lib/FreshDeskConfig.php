<?php
/**
 * lib/FreshDeskConfig.php
 */

/**
 * FreshDeskConfig.php
 *
 * A container for the entire Fresh Desk client configuration
 *
 */
class FreshDeskConfig {
    // Holds the entire ini config array
    public $configuration;

    // The Fresh Desk API URL and Authentication
    public $apiUrl;
    public $apiKey;
    public $apiPassword;
    

    // Fresh Desk Ticket Properties
    // https://developer.freshdesk.com/api/#quick-reference
    public $statusOpen;
    public $statusPending;
    public $statusResolved;
    public $statusClosed;


    /**
     * Initialize any dependency objects.
     */
    function __construct ()
    {
        // No dependencies
    }

    /**
     * Parse the standard .ini configuration file and store the data
     *
     * @param string $loadConfigFromFile The filepath to the ini config file.
     *
     * @return bool
     */
    public function loadConfigFromFile (string $pathToIniFile)
    {
        try
        {
            if(file_exists($pathToIniFile))
            {
                $this->configuration = parse_ini_file($pathToIniFile, true);
                $this->apiUrl      = $this->configuration["general"]["api_url"];
                $this->apiKey      = $this->configuration["authentication"]["api_key"];
                $this->apiPassword = $this->configuration["authentication"]["api_password"];

                // Status Codes
                $this->statusOpen     = $this->configuration["status"]["open"];
                $this->statusPending  = $this->configuration["status"]["pending"];
                $this->statusResolved = $this->configuration["status"]["resolved"];
                $this->statusClosed   = $this->configuration["status"]["closed"];
            }
            else
            {
                $errorMessage = "Configuration file not found.\r\n";
                throw new Exception($errorMessage);
            }
            return TRUE;
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }
}
?>