<?php
/*
 * lib/FreshDeskRequest.php
 */

/**
 * FreshDeskRequest
 *
 * An interface to the Fresh Desk 2.0 API
 *
 * DEPENDENCIES
 *      lib/FreshDeskConfig.php
 *      lib/HttpRequest.php
 *      
 *
 */
class FreshDeskRequest {
    public $freshDeskConfig;
    public $httpRequest;
    public $freshDeskApiUrl;
    public $lastRequestUrl;
    public $lastRequestId;
    public $requestLimitTotal;
    public $requestLimitRemaining;
    public $httpUsername;
    public $httpPassword;

    /**
     * This class depends on lib/FreshDeskConfig.php and lib/HttpRequest.php
     *
     * @param FreshDeskConfig $freshDeskConfig An instance of FreshDeskConfig.
     *
     * @param HttpRequest $httpRequest An instance of HttpRequest.
     *
     * @return void
     */
    function __construct(FreshDeskConfig $freshDeskConfig, HttpRequest $httpRequest)
    {
        $this->freshDeskConfig = $freshDeskConfig;
        $this->httpRequest     = $httpRequest;
    }

    /**
     * Validate the configuration and set the HTTP authentication.
     *
     * @param void
     *
     * @return bool
     */
    private function initRequest ()
    {
        try
        {
            if(empty($this->freshDeskConfig->apiUrl))
            {
                throw new Exception("No API URL was defined.\r\n");
            }
            elseif(empty($this->freshDeskConfig->apiKey))
            {
                throw new Exception("No API Key was defined.\r\n");
            }
            else
            {
                // Get he API URL from the configuration
                $this->freshDeskApiUrl = $this->freshDeskConfig->apiUrl;

                // Get the API Key from the configuration
                $this->httpUsername = $this->freshDeskConfig->apiKey;
                $this->httpPassword = $this->freshDeskConfig->apiPassword;

                // Set the authentication type for this request.
                $this->httpRequest->setHttpAuthBasic($this->httpUsername, $this->httpPassword);
            }
        }
        catch (Exception $e)
        {
            throw $e;
        }
        
    }

    /**
     * Use $this->httpRequest object to make a request to the Fresh Desk
     * service.  Then verify the response is sane and valid.  Return the
     * response body from the service.
     *
     * @param string $fullQueryUrl The full request URL.
     *
     * @return string The response from the service as a JSON string.
     */
    public function freshDeskGet (string $fullQueryUrl)
    {
        try
        {
            // Make the request from the Fresh Desk API
            $response = $this->httpRequest->httpGet($fullQueryUrl);

            if(!empty($response["curl_error"]))
            {
                // If the HTTP request itself failed for some reason, show the
                // error.
                $errorMessage  = "cURL Error: " . $response["curl_error"] . "\r\n";
                $errorMessage .= "Full Query URL: " . $fullQueryUrl . "\r\n";
                
                throw new Exception($errorMessage);
            }
            elseif($response["http_code"] === 200)
            {
                // The response was successful.
                if($response["headers"]["Content-Type"] == "application/json; charset=utf-8")
                {
                    $this->requestLimitTotal     = $response["headers"]["X-RateLimit-Total"];
                    $this->requestLimitRemaining = $response["headers"]["X-RateLimit-Remaining"];
                    $this->lastRequestId         = $response["headers"]["X-Request-Id"];
                    $this->lastRequestUrl        = $response["last_url"];
                    return $response["body"];
                }
                else
                {
                    // If the content-type isn't JSON, we don't know how to
                    // handle it yet.
                    $errorMessage  = "Service responded with an unexpected Content-Type: ";
                    $errorMessage .= $response["headers"]["Content-Type"];
                    $errorMessage .= "\r\n";
                    throw new Exception($errorMessage);
                }

            }
            else
            {
                // If the api returned an error, the body might potentially
                // have an error message that is useful.
                throw new Exception($response["body"] . "\r\n");
            }
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    /**
     * This uses a predefined Fresh Desk filter "new_and_my_open" to retrieve
     * all the tickets that are new and open for the authenticated user.
     *
     * @param void
     *
     * @return array The response body from Fresh Desk.
     */
    public function getNewMyOpen ()
    {
        // See: https://developer.freshdesk.com/api/#tickets
        $query = "tickets?filter=new_and_my_open";

        $this->initRequest();

        // Append the query to the API resource path.
        $fullQueryUrl = $this->freshDeskApiUrl . $query;
        
        return $this->freshDeskGet($fullQueryUrl);
    }

    /**
     * Get all the open tickets, up to 10 pages worth.  Attempting to get more
     * than 10 pages worth of open tickets will cause the service to return
     * an error.
     *
     * @param void
     *
     * @return string All open tickets as a JSON string.
     */
    public function getAllOpenTickets ()
    {
        $this->initRequest();
        $totalResponse = array();
        $statusOpen = $this->freshDeskConfig->statusOpen;
        $page = 1;
        do
        {
            $query = 'search/tickets?query="status:' . $statusOpen . '"&page=' . $page;
            $page++;

            // Append the query to the API resource path.
            $fullQueryUrl = $this->freshDeskApiUrl . $query;
            
            $pageResultJson = $this->freshDeskGet($fullQueryUrl);
            $pageResultArray = json_decode($pageResultJson, true);
            if (!empty($pageResultArray["results"]))
            {
                $totalResponse[] = $pageResultArray["results"];
            }
        }
        while (!empty($pageResultArray["results"]) and $page <= 10);
        
        return json_encode($totalResponse);
    }
}

?>
