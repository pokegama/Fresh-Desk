<?php
/*
 * test_fresh_desk.php
 */

require 'lib/FreshDeskConfig.php';
require 'lib/HttpRequest.php';
require 'lib/FreshDeskRequest.php';

/**
 * Get a list of IDs for all the open tickets.
 *
 * Param $subject
 *
 * Return array of ticket IDs
 * @param FreshDeskRequest freshDeskRequest A FreshDeskRequest object.
 *
 * @return string All open tickets
 */
function getAllOpenTicketIds ($freshDeskRequest)
{
    $ticketIds = array();
    $response = $freshDeskRequest->getAllOpenTickets();
    $allOpenTickets = json_decode($response, true);
    //var_dump($allOpenTickets);
    foreach($allOpenTickets as $pageOfTickets)
    {
        // If you do a var_dump() you can see that each $pageOfTickets is an
        // associative array 
        //var_dump($pageOfTickets);
        foreach($pageOfTickets as $ticket)
        {
            // And you can likewise see each ticket as an associative array.
            //var_dump($ticket);
            // Get the ID from each ticket and add it to the list of IDs.
            $ticketIds[] = $ticket["id"];
        }
    }
    return $ticketIds;
}

// ---------------------------------------------------------------------------
// MAIN
// ---------------------------------------------------------------------------
$configFile = "config.ini";
// FreshDeskRequest depends on FreshDeskConfig and HttpRequest
$freshDeskConfig = new FreshDeskConfig();
// Initialize the FreshDeskConfig object with data from the config file.
// From this point, certain attributes can be tweaked if desired.
$freshDeskConfig->loadConfigFromFile($configFile);
// You can still change the API Key before using this to create a new
// FreshDeskRequest.  Just uncomment the example and add your key.
//$freshDeskConfig->apiKey = "123abc";

$httpRequest      = new HttpRequest();
$freshDeskRequest = new FreshDeskRequest($freshDeskConfig, $httpRequest);

// Use one of the built-in Fresh Desk filters to get a list of tickets that are
// open and assigned to the authenticated user.
//$response = $freshDesk->getNewMyOpen();
//var_dump($response);

// Get all the open tickets as a big JSON string
//$response = $freshDeskRequest->getAllOpenTickets();
//var_dump($response);

// Use the getAllOpenTicketIds function above to get a list of all the IDs.
$response = getAllOpenTicketIds($freshDeskRequest);
var_dump($response);
print("Done.\r\n");

?>
